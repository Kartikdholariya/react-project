import React,{useState} from 'react';
import jQuery from 'jquery';
export default function TextForm(props) {
    const handleClick = () =>{
        let newText = text.toUpperCase();
        setText(newText);
    }
    const handleloClick = () =>{
        let newText = text.toLowerCase();
        setText(newText);
    }
    const handlesentance = () =>{
        let newText = text.trim().split(/[\.\?\!]\s/).length;
        jQuery('.totalsentance').html(newText);
    }
    const handleSpaces = ()=>{
        let newText = text.split(/[  ]+/);
        setText(newText.join(" "));
    }
    const handleOnChange = (event) =>{
        setText(event.target.value);
    }
    const [text, setText] = useState('');
    return (
        <>
        <div className='container'>
            <h1>{props.heading}</h1>
            <div className="mb-3">
                <textarea className="form-control" value={text} onChange={handleOnChange} id="myBox" rows="8"></textarea>
            </div>
            <button className="btn btn-primary mx-2" onClick={handleClick}>Convert to Uppercase</button>
            <button className="btn btn-primary mx-2" onClick={handleloClick}>Convert to Lowercase</button>
            <button className="btn btn-primary mx-2" onClick={handlesentance}>Number Of Sentance</button>
            <button className="btn btn-primary mx-2" onClick={handleSpaces}>Remove Extra Spaces</button>
        </div>
        <div className="container my-3">
            <h2>Your Text Summary</h2>
            <p>{text.split(" ").length} Words and {text.length} Characters</p>
            <p>{0.008 * text.split(" ").length} Minutes Read</p>
            <h3>Total Sentance</h3>
            <p className="totalsentance"></p>
            <h3>Preview</h3>
            <p>{text}</p>
        </div>
        </>
    )
}

