import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';
import TextForm from './components/TextForm';
import About from './components/About';

function App() {
  return (
    <>
    <Navbar title="My Apps" about="About Us"/>
    <div className="container my-3">
    <TextForm heading="Enter the text to analyze below"/>
    <About/>
    </div>
    {/* default properties <Navbar/>*/}
    </>
  );
}

export default App;
